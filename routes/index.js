const express = require('express');
const mongoose = require('mongoose');
const validator = require('validator');

const Url = require('../models/url');

const router = express.Router();

router.get('/', (req, res, next) => {
  res.send('Url Shortener Microservice');
});

router.get('/new/(*)', (req, res, next) => {
  let url = req.params[0];

  if (validator.isURL(url)) {
    //if url doesn't start with http or https - prefix it with http.
    const regex = /http(s)?:\/\/.*/;
    if (!url.match(regex)) {
      url = 'http://' + url;
    }
    Url.findOne({"url": url}, (err, data) => {
      if (err) {
        console.log(err);
      } else if (data) {
        res.json({
          short: data.short,
          original: data.url
        });
      } else {
        const short = Date.now();
        const newUrl = new Url({
          url: url,
          short: short
        });
        newUrl.save();
        res.json({
          short: short,
          original: url
        });
      }
    });
  } else {
    res.json({
      error: "Wrong url format, make sure you have a valid protocol and real site."
    });
  }
});

router.get('/:id', (req, res, next) => {
  Url.findOne({"short": req.params.id}, (err, data) => {
    if (err) {
      console.log(err);
    } else if (!data) {
      res.json({
        error: "No record in DB."
      });
    } else {
      res.redirect(data.url);
    }
  });
});

module.exports = router;
