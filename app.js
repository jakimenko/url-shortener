const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const favicon = require('serve-favicon')
const path = require('path');

const index = require('./routes/index');

const app = express();

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

mongoose.connect(process.env.MONGO_URL);

app.use(logger('dev'));

app.use('/', index);

module.exports = app;
